# translation of libkonq.po to
# Copyright (C) 2001,2002, 2004, 2006, 2007 Free Software Foundation, Inc.
# Nuriddin Aminagha <nuriddin@haydarlinux.org>
# Hicham amaoui <amaoui@altern.org> 2003
#
# Mohammed Gamal <f2c2001@yahoo.com>, 2001.
# Sayed Jaffer Al-Mosawi <mosawi@arabeyes.org>, 2002.
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002.
# Ossama Khayat <okhayat@yahoo.com>, 2004.
# Munzir Taha <munzir@kacst.edu.sa>, 2004.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# AbdulAziz AlSharif <a.a-a.s@hotmail.com>, 2007.
# Youssef Chahibi <chahibi@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: libkonq\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-04-04 11:57+0200\n"
"PO-Revision-Date: 2007-10-14 15:38+0000\n"
"Last-Translator: Youssef Chahibi <chahibi@gmail.com>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Poedit-Language: Arabic\n"

#: knewmenu.cpp:139
msgid "Create New"
msgstr "أنشئ جديد"

#: knewmenu.cpp:161
msgid "Link to Device"
msgstr "رابط لجهاز"

#: knewmenu.cpp:449
#, kde-format
msgid "<qt>The template file <b>%1</b> does not exist.</qt>"
msgstr "<qt>ملف القالب <b>%1</b> غير موجود مسبقاّ.</qt>"

#: knewmenu.cpp:462
msgid "File name:"
msgstr "اسم الملف:"

#: konq_fileundomanager.cpp:85
#, fuzzy
msgid "Creating directory"
msgstr "يجري إنشاء دليل"

#: konq_fileundomanager.cpp:86
#, fuzzy
msgid "Directory"
msgstr "دليل"

#: konq_fileundomanager.cpp:88
#, fuzzy
msgid "Moving"
msgstr "جاري النقل"

#: konq_fileundomanager.cpp:89
#, fuzzy
msgid "Source"
msgstr "المصدر"

#: konq_fileundomanager.cpp:90
#, fuzzy
msgid "Destination"
msgstr "الوجهة"

#: konq_fileundomanager.cpp:92
#, fuzzy
msgid "Deleting"
msgstr "جاري الحذف"

#: konq_fileundomanager.cpp:93
#, fuzzy
msgid "File"
msgstr "ملف"

#: konq_fileundomanager.cpp:274
msgid "Und&o"
msgstr "ت&راجع"

#: konq_fileundomanager.cpp:278
msgid "Und&o: Copy"
msgstr "ت&راجع: انسخ"

#: konq_fileundomanager.cpp:280
msgid "Und&o: Link"
msgstr "ت&راجع: اربط"

#: konq_fileundomanager.cpp:282
msgid "Und&o: Move"
msgstr "ت&راجع: انقل"

#: konq_fileundomanager.cpp:284
msgid "Und&o: Rename"
msgstr "ت&راجع: أعد تسمية"

#: konq_fileundomanager.cpp:286
msgid "Und&o: Trash"
msgstr "ت&راجع: سلة المهملات"

#: konq_fileundomanager.cpp:288
msgid "Und&o: Create Folder"
msgstr "ت&راجع: إنشئ مجلد"

#: konq_fileundomanager.cpp:733
#, fuzzy, kde-format
msgid ""
"The file %1 was copied from %2, but since then it has apparently been "
"modified at %3.\n"
"Undoing the copy will delete the file, and all modifications will be lost.\n"
"Are you sure you want to delete %4?"
msgstr "الـ ملفّ من منذ الإيطالية معدّل عند نسخ حذف ملفّ و الكل إلى حذف?"

#: konq_fileundomanager.cpp:736
msgid "Undo File Copy Confirmation"
msgstr "تراجع تأكيد نسخ الملف"

#: konq_menuactions.cpp:397
#, fuzzy
#| msgid "Ac&tions"
msgctxt "@title:menu"
msgid "Actions"
msgstr "إجراءات"

#: konq_operations.cpp:274
#, fuzzy, kde-format
msgid "Do you really want to delete this item?"
msgid_plural "Do you really want to delete these %1 items?"
msgstr[0] " هل تريد فعلا حذف هذه العناصر؟"
msgstr[1] " هل تريد فعلا حذف هذه العناصر؟"
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""
msgstr[5] ""

#: konq_operations.cpp:276
msgid "Delete Files"
msgstr "إحذف الملفات"

#: konq_operations.cpp:286
#, fuzzy, kde-format
msgid "Do you really want to move this item to the trash?"
msgid_plural "Do you really want to move these %1 items to the trash?"
msgstr[0] "تنفيذ إلى انقل عنصر إلى سلة المهملات?"
msgstr[1] "تنفيذ إلى انقل عنصر إلى سلة المهملات?"
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""
msgstr[5] ""

#: konq_operations.cpp:288
msgid "Move to Trash"
msgstr "انقل إلى سلة المهملات"

#: konq_operations.cpp:289
#, fuzzy
msgctxt "Verb"
msgid "&Trash"
msgstr "ا&رم"

#: konq_operations.cpp:334
msgid "You cannot drop a folder on to itself"
msgstr "لا يمكن إسقاط المجلّد على نفسه"

#: konq_operations.cpp:375
msgid "File name for dropped contents:"
msgstr "اسم الملف للمحتويات المفقودة:"

#: konq_operations.cpp:557
msgid "&Move Here"
msgstr "ان&قل هنا"

#: konq_operations.cpp:561
msgid "&Copy Here"
msgstr "ا&نسخ هنا"

#: konq_operations.cpp:565
msgid "&Link Here"
msgstr "ا&ربط هنا"

#: konq_operations.cpp:567
msgid "Set as &Wallpaper"
msgstr "عيّن كـ خ&لفية شاشة"

#: konq_operations.cpp:569
msgid "C&ancel"
msgstr "إ&لغي"

#: konq_operations.cpp:752 konq_operations.cpp:754 konq_operations.cpp:756
msgid "New Folder"
msgstr "مجلد جديد"

#: konq_operations.cpp:757
msgid "Enter folder name:"
msgstr "أدخل اسم المجلد:"

#: konq_popupmenu.cpp:329
msgid "&Open"
msgstr "ا&فتح"

#: konq_popupmenu.cpp:329
msgid "Open in New &Window"
msgstr "افتح في &نافذة جديدة"

#: konq_popupmenu.cpp:339
msgid "Open the trash in a new window"
msgstr "افتح سلة المهملات في نافذة جديدة"

#: konq_popupmenu.cpp:341
msgid "Open the medium in a new window"
msgstr "افتح الوسيط في نافذة جديدة"

#: konq_popupmenu.cpp:343
msgid "Open the document in a new window"
msgstr "افتح المستند في نافذة جديدة"

#: konq_popupmenu.cpp:363
msgid "Create &Folder..."
msgstr "إنشئ &مجلد..."

#: konq_popupmenu.cpp:372
msgid "&Restore"
msgstr "&استعد"

#: konq_popupmenu.cpp:414
msgid "&Empty Trash Bin"
msgstr "أ&فرغ سلة المهملات"

#: konq_popupmenu.cpp:440
msgid "&Bookmark This Page"
msgstr "ضع علامة مرج&عية لهذه الصفحة"

#: konq_popupmenu.cpp:442
msgid "&Bookmark This Location"
msgstr "ضع علامة مرجع&ية لهذا الموقع"

#: konq_popupmenu.cpp:445
msgid "&Bookmark This Folder"
msgstr "ضع علامة مرج&عية لهذا المجلد"

#: konq_popupmenu.cpp:447
msgid "&Bookmark This Link"
msgstr "ضع علامة مرج&عية لهذا الرابط"

#: konq_popupmenu.cpp:449
msgid "&Bookmark This File"
msgstr "ضع علامة مرج&عية لهذا الملف"

#: konq_popupmenu.cpp:500
msgid "&Open With"
msgstr "ا&فتح بواسطة"

#: konq_popupmenu.cpp:529
#, kde-format
msgid "Open with %1"
msgstr "افتح بواسطة %1"

#: konq_popupmenu.cpp:543
msgid "&Other..."
msgstr "آ&خر..."

#: konq_popupmenu.cpp:545 konq_popupmenu.cpp:555
msgid "&Open With..."
msgstr "ا&فتح بواسطة..."

#: konq_popupmenu.cpp:566
#, fuzzy
#| msgid "Preview"
msgid "Preview In"
msgstr "معاينة في"

#: konq_popupmenu.cpp:585
msgid "&Properties"
msgstr "&خصائص"

#: konq_popupmenu.cpp:598
msgid "Share"
msgstr "مشترك"
