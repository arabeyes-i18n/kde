# translation of plasma_applet_desktop.po to Arabic
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Youssef Chahibi <chahibi@gmail.com>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_desktop\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-04-12 14:01+0200\n"
"PO-Revision-Date: 2008-01-01 13:37+0000\n"
"Last-Translator: Youssef Chahibi <chahibi@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: backgrounddialog.cpp:331
#, kde-format
msgctxt "Caption to wallpaper preview, %1 author name"
msgid "by %1"
msgstr ""

#: backgrounddialog.cpp:364
msgid "Configure Desktop"
msgstr "اضبط سطح المكتب"

#: backgrounddialog.cpp:384
#, fuzzy
#| msgid "Scale and crop"
msgid "Scale & Crop"
msgstr "محجّم ومقطّع"

#: backgrounddialog.cpp:386
msgid "Scaled"
msgstr "محجّم"

#: backgrounddialog.cpp:388
msgid "Centered"
msgstr "في الوسط"

#: backgrounddialog.cpp:390
msgid "Tiled"
msgstr "مبلّط"

#: backgrounddialog.cpp:392
#, fuzzy
#| msgid "Center tiled"
msgid "Center Tiled"
msgstr "مبلّط في الوسط"

#: backgrounddialog.cpp:422
msgid ""
"This picture of a monitor contains a preview of what the current settings "
"will look like on your desktop."
msgstr "صورة المرقاب تعطي لمحة عن الشكل المرتقب لسطح المكتب."

#: backgrounddialog.cpp:527
#, fuzzy
#| msgid "Select a wallpaper image file"
msgid "Select Wallpaper Image File"
msgstr "اختر ملفا لصورة الخلفية"

#: backgroundpackage.cpp:140
msgid "Images"
msgstr "الصور"

#: backgroundpackage.cpp:141
msgid "Screenshot"
msgstr "لقطة"

#: desktop.cpp:145
msgid "Unlock Widgets"
msgstr "فك القفل عن الأدوات"

#: desktop.cpp:148 desktop.cpp:350
msgid "Lock Widgets"
msgstr "اقفل الأدوات"

#: desktop.cpp:338
msgid "Add Widgets..."
msgstr "أضف عناصرا."

#: desktop.cpp:342
msgid "Run Command..."
msgstr "نفّذ الأمر..."

#: desktop.cpp:346
msgid "Configure Desktop..."
msgstr "اضبط سطح المكتب..."

#: desktop.cpp:354
msgid "Lock Screen"
msgstr "اقفل الشاشة"

#: desktop.cpp:358
msgid "Logout"
msgstr "اخرج"

#: iconloader.cpp:67
msgid "Icon"
msgstr "أيقونة"

#: iconloader.cpp:84
msgid "Align Horizontally"
msgstr ""

#: iconloader.cpp:88
msgid "Align Vertically"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 27
#: rc.cpp:3
msgid "Wallpaper Image"
msgstr "صورة الخلفية"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 32
#: rc.cpp:6
msgid "Slideshow"
msgstr "عرض متغيّر"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 67
#: rc.cpp:9
#, fuzzy
#| msgid "Static Picture"
msgid "&Picture:"
msgstr "صورة ساكنة"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 90
#: rc.cpp:12
msgid "Browse"
msgstr "تصفّح"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 93
#: rc.cpp:15
#, fuzzy
#| msgid "&Add..."
msgid "..."
msgstr "أ&ضف..."

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 117
#: rc.cpp:18
msgid "P&ositioning:"
msgstr "المو&ضع:"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 140
#: rc.cpp:21
msgid "&Color:"
msgstr "ال&لون:"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 196
#: rc.cpp:24
msgid ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">Author:</span></p></body></html>"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 212
#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 232
#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 258
#: rc.cpp:30 rc.cpp:39 rc.cpp:48
msgid "TextLabel"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 222
#: rc.cpp:33
msgid ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">E-Mail:</span></p></body></html>"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 242
#: rc.cpp:42
msgid ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:10pt; font-"
"weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-"
"weight:600;\">License:</span></p></body></html>"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 276
#: rc.cpp:51
#, fuzzy
#| msgid "&Add directory..."
msgid "&Add Directory..."
msgstr "أ&ضف مجلّدا..."

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 286
#: rc.cpp:54
#, fuzzy
#| msgid "&Remove directory"
msgid "&Remove Directory..."
msgstr "أ&زل مجلّدا"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 315
#: rc.cpp:57
msgid "&Change images every:"
msgstr "&غيّر الصورة كلّ:"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 338
#: rc.cpp:60
#, fuzzy
#| msgid "h:mm:ss"
msgid "hh:mm:ss"
msgstr "h:mm:ss"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 375
#: rc.cpp:63
msgid "Monitor"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 410
#: rc.cpp:66
msgid "Download new wallpapers"
msgstr "نزّل صورا جديدة"

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 413
#: rc.cpp:69
msgid "New Wallpaper..."
msgstr "صورة خلفية جديدة."

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 454
#: rc.cpp:72
msgid "&Show icons"
msgstr ""

#. i18n: tag string
#. i18n: file BackgroundDialog.ui line 471
#: rc.cpp:75
msgid "&Align to grid"
msgstr ""

#~ msgid "Url"
#~ msgstr "المسار"

#, fuzzy
#~| msgid "Author"
#~ msgid "Author:"
#~ msgstr "المؤلف"

#, fuzzy
#~| msgid "E-mail"
#~ msgid "E-Mail:"
#~ msgstr "البريد"

#~ msgid "&New wallpaper..."
#~ msgstr "خلفية &جديدة..."

#~ msgid "&Background Color"
#~ msgstr "اللون الخلفي"

#~ msgid "SlideShow"
#~ msgstr "صور متغيّرة"

#~ msgid "*.png *.jpeg *.jpg *.svg *.svgz"
#~ msgstr "*.png *.jpeg *.jpg *.svg *.svgz"

#~ msgid "&Remove..."
#~ msgstr "&احذف..."

#~ msgid "Show images from:"
#~ msgstr "أظهر صورا من:"

#~ msgid "Move &Up"
#~ msgstr "للأ&على"

#~ msgid "Move &Down"
#~ msgstr "للأ&سفل"

#~ msgid "Change images every:"
#~ msgstr "غيّر الصور كل:"
