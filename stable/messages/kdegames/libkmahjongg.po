# translation of libkmahjongg.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Youssef Chahibi <chahibi@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: libkmahjongg\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2007-10-10 05:31+0200\n"
"PO-Revision-Date: 2007-10-14 15:45+0000\n"
"Last-Translator: Youssef Chahibi <chahibi@gmail.com>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: kmahjonggconfigdialog.cpp:57
msgid "Tiles"
msgstr "القراميد"

#: kmahjonggconfigdialog.cpp:64
msgid "Background"
msgstr "الخلفية"

#. i18n: tag string
#. i18n: file kmahjonggbackgroundselector.ui line 46
#. i18n: tag string
#. i18n: file kmahjonggtilesetselector.ui line 46
#: rc.cpp:3 rc.cpp:18
msgid "Preview"
msgstr "معاينة"

#. i18n: tag string
#. i18n: file kmahjonggbackgroundselector.ui line 81
#. i18n: tag string
#. i18n: file kmahjonggtilesetselector.ui line 81
#: rc.cpp:6 rc.cpp:21
msgid "Properties"
msgstr "خصائص"

#. i18n: tag string
#. i18n: file kmahjonggbackgroundselector.ui line 101
#. i18n: tag string
#. i18n: file kmahjonggtilesetselector.ui line 101
#: rc.cpp:9 rc.cpp:24
msgid "Author:"
msgstr "المؤلف:"

#. i18n: tag string
#. i18n: file kmahjonggbackgroundselector.ui line 111
#. i18n: tag string
#. i18n: file kmahjonggtilesetselector.ui line 111
#: rc.cpp:12 rc.cpp:27
msgid "Contact:"
msgstr "الإتصال:"

#. i18n: tag string
#. i18n: file kmahjonggbackgroundselector.ui line 121
#. i18n: tag string
#. i18n: file kmahjonggtilesetselector.ui line 121
#: rc.cpp:15 rc.cpp:30
msgid "Description:"
msgstr "الوصف:"
