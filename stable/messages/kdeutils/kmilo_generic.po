# translation of kmilo_generic.po to
#
# Khaled Hosny <dr.khaled.hosny@gmail.com>, 2006.
# Youssef Chahibi <chahibi@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kmilo_generic\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2007-03-23 18:05+0100\n"
"PO-Revision-Date: 2007-10-14 16:01+0000\n"
"Last-Translator: Youssef Chahibi <chahibi@gmail.com>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: generic_monitor.cpp:107 generic_monitor.cpp:194
msgid "Starting KMix..."
msgstr "بدأ تشغيل KMix..."

#: generic_monitor.cpp:124 generic_monitor.cpp:216
msgid "It seems that KMix is not running."
msgstr "يبدو أن KMix لا يعمل حالياً."

#: generic_monitor.cpp:167
msgid "Volume"
msgstr "الحجم"

#: generic_monitor.cpp:236
msgid "Mute on"
msgstr "قطع الصوت"

#: generic_monitor.cpp:239
msgid "Mute off"
msgstr "تشغيل الصوت"
