# translation of libmailtransport.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Youssef Chahibi <chahibi@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: libmailtransport\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2008-01-02 06:21+0100\n"
"PO-Revision-Date: 2007-10-14 15:58+0000\n"
"Last-Translator: Youssef Chahibi <chahibi@gmail.com>\n"
"Language-Team:  <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: KBabel 1.11.4\n"

#: precommandjob.cpp:68
#, fuzzy
msgid "Executing precommand"
msgstr "التنفيذ"

#: precommandjob.cpp:69
#, fuzzy, kde-format
msgid "Executing precommand '%1'."
msgstr "التنفيذ."

#: precommandjob.cpp:75
#, fuzzy, kde-format
msgid "Could not execute precommand '%1'."
msgstr "غير ممكن تنفيذ الأمر التمهيدي '%1'."

#: precommandjob.cpp:92
#, fuzzy
msgid "The precommand crashed."
msgstr "الـ."

#: precommandjob.cpp:95
#, fuzzy, kde-format
msgid "The precommand exited with code %1."
msgstr "الـ مع رمز."

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 12
#: rc.cpp:3
#, fuzzy
msgid "Unique identifier"
msgstr "وحيدفريد معرَف"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 16
#: rc.cpp:6
#, fuzzy
msgid "User visible transport name"
msgstr "المستخدم مرئي نقل الإسم"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 17
#: rc.cpp:9
#, fuzzy
msgid "The name that will be used when referring to this server."
msgstr "الـ الإسم مُستخدَم إلى خادم."

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 23
#: rc.cpp:12
#, fuzzy
msgid "SMTP Server"
msgstr "خادم SMTP"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 26
#: rc.cpp:15
#, fuzzy
msgid "Local sendmail"
msgstr "محلي sendmail"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 29
#: rc.cpp:18
#, fuzzy
msgid "Transport type"
msgstr "نقل نوع"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 33
#: rc.cpp:21
#, fuzzy
msgid "Host name of the server"
msgstr "مضيف الإسم من خادم"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 34
#: rc.cpp:24
#, fuzzy
msgid "The domain name or numerical address of the SMTP server."
msgstr "إسم المجال أو العنوان الرقمي للخادم SMTP."

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 37
#: rc.cpp:27
#, fuzzy
msgid "Port number of the server"
msgstr "منفذ رقم من خادم"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 38
#: rc.cpp:30
#, fuzzy
msgid ""
"The port number that the SMTP server is listening on. The default port is 25."
msgstr "رقم المنفذ اللذي يستمع عليه الخادم SMTP. المنفذ الإفتراضي رقمه 25."

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 42
#: rc.cpp:33
#, fuzzy
msgid "User name needed for login"
msgstr "المستخدم الإسم لـ دخول ، تسجيل"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 43
#: rc.cpp:36
#, fuzzy
msgid "The user name to send to the server for authorization."
msgstr ""
"الـ مستخدم الإسم إلى إرسال إلى خادم لـ تخويل\n"
"ترخيص."

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 46
#: rc.cpp:39
#, fuzzy
msgid "Command to execute before sending a mail"
msgstr "الأمر إلى نفِّذ قبل a بريد"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 51
#: rc.cpp:42
#, fuzzy
msgid ""
"\n"
"        A command to run locally, prior to sending email.\n"
"        This can be used to set up SSH tunnels, for example.\n"
"        Leave it empty if no command should be run.\n"
"      "
msgstr ""
"\n"
" A أمر إلى تنفيذ إلى بريد إلكتروني\n"
" هذا مُستخدَم إلى set أعلى SSH لـ\n"
" غادِر الإيطالية فارغ IF لا أمر تنفيذ n "

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 54
#: rc.cpp:49
#, fuzzy
msgid "Server requires authentication"
msgstr "الخادم توثق"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 58
#: rc.cpp:52
#, fuzzy
msgid ""
"\n"
"        Check this option if your SMTP server requires authentication before "
"accepting mail.\n"
"        This is known as 'Authenticated SMTP' or simply ASMTP.\n"
"      "
msgstr ""
"\n"
" كِش خيار IF SMTP خادم توثق قبل بريد\n"
" هذا هو صدّق SMTP أو n "

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 62
#: rc.cpp:58
#, fuzzy
msgid "Store password"
msgstr "تخزين كلمة مرور"

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 68
#: rc.cpp:61
#, fuzzy
msgid ""
"\n"
"        Check this option to have your password stored.\n"
"        \\nIf KWallet is available the password will be stored there which "
"is considered safe.\\n\n"
"        However, if KWallet is not available, the password will be stored in "
"the configuration file.\n"
"        The password is stored in an obfuscated format, but should not be "
"considered secure from decryption efforts if access to the configuration "
"file is obtained.\n"
"      "
msgstr ""
"\n"
" كِش خيار إلى كلمة مرور\n"
" المحفظة KWallet هو متوفّر كلمة مرور هو آمن n\n"
" على أية حال IF المحفظة KWallet هو ليس متوفّر كلمة مرور بوصة تشكيل ملفّ\n"
" الـ كلمة مرور هو بوصة تنسيق ليس آمن من فك تشفير IF وصول إلى تشكيل ملفّ هو n "

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 72
#: rc.cpp:69
#, fuzzy
msgid "Encryption method used for communication"
msgstr "التشفير طريقة مُستخدَم لـ تواصل"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 75
#: rc.cpp:72
#, fuzzy
msgid "No encryption"
msgstr "لا تشفير"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 78
#: rc.cpp:75
#, fuzzy
msgid "SSL encryption"
msgstr "تشفير SSL "

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 81
#: rc.cpp:78
#, fuzzy
msgid "TLS encryption"
msgstr "تشفير TLS"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 86
#: rc.cpp:81
#, fuzzy
msgid "Authentication method"
msgstr "طريقة المواثقة"

#. i18n: tag label
#. i18n: file mailtransport.kcfg line 99
#. i18n: tag label
#. i18n: file mailtransport.kcfg line 107
#: rc.cpp:84 rc.cpp:93
msgid "<!-- TODO -->"
msgstr ""

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 103
#: rc.cpp:87
#, fuzzy
msgid ""
"\n"
"        Check this option to use a custom hostname when identifying to the "
"mail server.\n"
"        <p/>This is useful when your system's hostname may not be set "
"correctly or to mask your system's true hostname.\n"
"      "
msgstr ""
"\n"
" كِش خيار إلى استخدام a مخصص اسم مضيف إلى بريد خادم\n"
"<p/> هذا هو نظام s اسم مضيف أيار ليس set أو إلى حجاب ، قناع نظام s صحيح اسم "
"مضيف n "

#. i18n: tag whatsthis
#. i18n: file mailtransport.kcfg line 108
#: rc.cpp:96
#, fuzzy
msgid "Enter the hostname that should be used when identifying to the server."
msgstr "إدخال اسم مضيف مُستخدَم إلى خادم."

#. i18n: tag string
#. i18n: file sendmailsettings.ui line 45
#: rc.cpp:99
#, fuzzy
msgid "Transport: Sendmail"
msgstr "النّقل: Sendmail"

#. i18n: tag string
#. i18n: file sendmailsettings.ui line 52
#: rc.cpp:102
#, fuzzy
msgid "&Location:"
msgstr "ال&موقع:"

#. i18n: tag string
#. i18n: file sendmailsettings.ui line 62
#. i18n: tag string
#. i18n: file smtpsettings.ui line 45
#: rc.cpp:105 rc.cpp:117
#, fuzzy
msgid "&Name:"
msgstr "الإ&سم:"

#. i18n: tag string
#. i18n: file sendmailsettings.ui line 72
#: rc.cpp:108
#, fuzzy
msgid "Choos&e..."
msgstr "إخت&ر..."

#. i18n: tag string
#. i18n: file smtpsettings.ui line 23
#: rc.cpp:111
#, fuzzy
msgid "Transport: SMTP"
msgstr "النّقل: ميفاق نقل البريد البسيط SMTP"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 37
#: rc.cpp:114
#, fuzzy
msgid "&General"
msgstr "&عام"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 61
#: rc.cpp:120
#, fuzzy
msgid "&Host:"
msgstr "الم&ضيف:"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 77
#: rc.cpp:123
#, fuzzy
msgid "&Port:"
msgstr "ال&منفذ:"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 118
#: rc.cpp:126
#, fuzzy
msgid "Encryption"
msgstr "التشفير"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 126
#: rc.cpp:129
#, fuzzy
msgid "&None"
msgstr "&بدون"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 133
#: rc.cpp:132
#, fuzzy
msgid "&SSL"
msgstr "&SSL"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 140
#: rc.cpp:135
#, fuzzy
msgid "&TLS"
msgstr "&TLS"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 149
#: rc.cpp:138
#, fuzzy
msgid "Check &What the Server Supports"
msgstr "تحقق من &ما يدعم الخادم"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 195
#: rc.cpp:141
#, fuzzy
msgid "&Advanced"
msgstr "مت&قدم"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 210
#: rc.cpp:144
#, fuzzy
msgid "This server does not support authentication"
msgstr "هذا خادم ليس دعم توثق"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 220
#: rc.cpp:147
#, fuzzy
msgid "Server &requires authentication"
msgstr "&يحتاج الخادم إلى التوثيق"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 232
#: rc.cpp:150
#, fuzzy
msgid "&Login:"
msgstr "إسم الد&خول:"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 255
#: rc.cpp:153
#, fuzzy
msgid "P&assword:"
msgstr "&كلمة المرور:"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 271
#: rc.cpp:156
#, fuzzy
msgid "The password to send to the server for authorization."
msgstr ""
"الـ كلمة مرور إلى إرسال إلى خادم لـ تخويل\n"
"ترخيص."

#. i18n: tag string
#. i18n: file smtpsettings.ui line 284
#: rc.cpp:159
#, fuzzy
msgid "&Store SMTP password"
msgstr "&إحفظ كلمة المرور SMTP"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 294
#: rc.cpp:162
#, fuzzy
msgid "Authentication Method"
msgstr "طريقة التحقق"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 302
#: rc.cpp:165
#, fuzzy
msgid "&LOGIN"
msgstr "&LOGIN"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 309
#: rc.cpp:168
#, fuzzy
msgid "&PLAIN"
msgstr "&PLAIN"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 316
#: rc.cpp:171
#, fuzzy
msgid "CRAM-MD&5"
msgstr "CRAM-MD&5"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 323
#: rc.cpp:174
#, fuzzy
msgid "&DIGEST-MD5"
msgstr "&DIGEST-MD5"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 330
#: rc.cpp:177
#, fuzzy
msgid "&GSSAPI"
msgstr "&GSSAPI"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 337
#: rc.cpp:180
#, fuzzy
msgid "&NTLM"
msgstr "&NTLM"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 354
#: rc.cpp:183
#, fuzzy
msgid "Sen&d custom hostname to server"
msgstr "أ&رسل إسم المضيف  المعتاد إلى الخادم"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 366
#: rc.cpp:186
#, fuzzy
msgid "Hos&tname:"
msgstr "إسم ال&مضيف:"

#. i18n: tag string
#. i18n: file smtpsettings.ui line 393
#: rc.cpp:189
#, fuzzy
msgid "Precommand:"
msgstr "قبل القيادة:"

#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 35
#: rc.cpp:192
#, fuzzy
msgid "Set Default"
msgstr "تعيين الافتراضي"

#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 42
#: rc.cpp:195
#, fuzzy
msgid "R&emove"
msgstr "إ&زالة"

#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 49
#: rc.cpp:198
#, fuzzy
msgid "&Modify..."
msgstr "&عدّل..."

#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 56
#: rc.cpp:201
#, fuzzy
msgid "A&dd..."
msgstr "أ&ضف..."

#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 82
#. i18n: tag string
#. i18n: file transportmanagementwidget.ui line 87
#: rc.cpp:204 rc.cpp:207
#, fuzzy
msgid "1"
msgstr "1"

#. i18n: tag string
#. i18n: file transporttypedialog.ui line 23
#: rc.cpp:210
#, fuzzy
msgid "Transport Type"
msgstr "نقل النوع"

#. i18n: tag string
#. i18n: file transporttypedialog.ui line 38
#: rc.cpp:213
#, fuzzy
msgid "SM&TP"
msgstr "SM&TP"

#. i18n: tag string
#. i18n: file transporttypedialog.ui line 48
#: rc.cpp:216
#, fuzzy
msgid "&Sendmail"
msgstr "&Sendmail"

#: sendmailjob.cpp:72
#, fuzzy, kde-format
msgid "Failed to execute mailer program %1"
msgstr "فشل في تنفيذ برنامج mailer %1"

#: sendmailjob.cpp:86
#, fuzzy
msgid "Sendmail exited abnormally."
msgstr "انتهى البرنامج Sendmail بشكل غير طبيعي."

#: sendmailjob.cpp:88
#, fuzzy, kde-format
msgid "Sendmail exited abnormally: %1"
msgstr "Sendmail 1"

#: smtpjob.cpp:148
#, fuzzy
msgid "You need to supply a username and a password to use this SMTP server."
msgstr "أنت تحتاج إلى إسم مستخدم وكلمة مرور لاستخدام هذا الخادم SMTP."

#: smtpjob.cpp:194
#, fuzzy
msgid "Unable to create SMTP job."
msgstr "عاجز إلى إ_نشيء SMTP شغل."

#: transport.cpp:146
#, fuzzy, kde-format
msgid ""
"KWallet is not available. It is strongly recommended to use KWallet for "
"managing your passwords.\n"
"However, the password can be stored in the configuration file instead. The "
"password is stored in an obfuscated format, but should not be considered "
"secure from decryption efforts if access to the configuration file is "
"obtained.\n"
"Do you want to store the password for server '%1' in the configuration file?"
msgstr ""
"المحفظة KWallet هو ليس متوفّر itإيطالياهو هو مفضّل إلى استخدام المحفظة KWallet "
"لـ كلمة مرور بوصة تشكيل ملفّ الـ كلمة مرور هو بوصة تنسيق ليس آمن من فك تشفير "
"IF وصول إلى تشكيل ملفّ هو إلى تخزين كلمة مرور لـ خادم بوصة تشكيل ملفّ?"

#: transport.cpp:154
#, fuzzy
msgid "KWallet Not Available"
msgstr "البرنامج KWallet غير متوفر"

#: transport.cpp:155
#, fuzzy
msgid "Store Password"
msgstr "إحفظ كلمة المرور"

#: transport.cpp:156
#, fuzzy
msgid "Do Not Store Password"
msgstr "لا تحفظ كلمة المرور"

#: transportconfigdialog.cpp:210
#, fuzzy, kde-format
msgctxt ""
"%1: name; %2: number appended to it to make it unique among a list of names"
msgid "%1 %2"
msgstr "%1 %2"

#: transportconfigdialog.cpp:222
#, fuzzy
msgid "Choose sendmail Location"
msgstr "إختار موقع البرنامج sendmail"

#: transportconfigdialog.cpp:229
#, fuzzy
msgid "Only local files allowed."
msgstr "مسموح فقط الملفات المحليّة."

#: transportjob.cpp:125
#, fuzzy, kde-format
msgid "The mail transport \"%1\" is not correcty configured."
msgstr "الـ بريد نقل هو ليس مُهيّء."

#: transportmanagementwidget.cpp:45
#, fuzzy
msgid "Name"
msgstr "الاسم"

#: transportmanagementwidget.cpp:45
#, fuzzy
msgid "Type"
msgstr "النوع"

#: transportmanagementwidget.cpp:80
#, fuzzy
msgid "SMTP"
msgstr "SMTP"

#: transportmanagementwidget.cpp:83
#, fuzzy
msgid "Sendmail"
msgstr "Sendmail"

#: transportmanagementwidget.cpp:87
#, fuzzy
msgid " (Default)"
msgstr " ( الإفتراضي )"

#: transportmanagementwidget.cpp:128 transporttypedialog.cpp:41
#, fuzzy
msgid "Add Transport"
msgstr "إضافة طريقة نقل"

#: transportmanagementwidget.cpp:146
#, fuzzy
msgid "Modify Transport"
msgstr "غير طريقة النقل"

#: transportmanager.cpp:177
#, fuzzy
msgid "Default Transport"
msgstr "افتراضي نقل"

#: transportmanager.cpp:508
#, fuzzy
msgid ""
"The following mail transports store passwords in the configuration file "
"instead in KWallet.\n"
"It is recommended to use KWallet for password storage for security reasons.\n"
"Do you want to migrate your passwords to KWallet?"
msgstr ""
"الـ متابعة بريد تخزين بوصة تشكيل ملفّ بوصة المحفظة KWallet هو مفضّل إلى "
"استخدام المحفظة KWallet لـ كلمة مرور تخزين لـ أمن إلى إلى المحفظة KWallet?"

#: transportmanager.cpp:512
#, fuzzy
msgid "Question"
msgstr "سؤال"

#: transportmanager.cpp:513
#, fuzzy
msgid "Migrate"
msgstr "هاجر"

#: transportmanager.cpp:513
#, fuzzy
msgid "Keep"
msgstr "حافظ عليه"
