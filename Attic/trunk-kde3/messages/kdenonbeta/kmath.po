# translation of kmath.po to Arabic
# Copyright (C) 2003, 2006 Free Software Foundation, Inc.
#
# Nuriddin Aminagha <nuriddin@eminaga.de>, 2003.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kmath\n"
"POT-Creation-Date: 2005-06-13 01:34+0200\n"
"PO-Revision-Date: 2006-11-02 16:29+0100\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr "محمد سعد Mohamed SAAD"

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr "metehyi@free.fr"

#: edu_factory.cpp:122 edu_factory.cpp:139
msgid ""
"There was an error loading the module %1.\n"
"The diagnostics is:\n"
"%2"
msgstr ""
"لقد كان هناك خطأ أثناء تحميل الوحدة %1.\n"
"السبب المرجح هو:\n"
"%2"

#: edu_factory.cpp:202 main.cpp:23
msgid "KMath"
msgstr "KMath"

#: edu_factory.cpp:204
msgid "Edutainment program"
msgstr "برنامج تعليم ترفيهي"

#: edu_factory.cpp:206
msgid "(c) 1999-2002, The Edu developers"
msgstr "(c) 1999-2002, The Edu developers"

#: edu_factory.cpp:208
msgid "http://edu.kde.org"
msgstr "http://edu.kde.org"

#: edu_factory.cpp:209
msgid "developer and maintainer"
msgstr "المشرف والمطور"

#: main.cpp:11
msgid "A KDE KPart Application"
msgstr "تطبيق من KDE KPart"

#: main.cpp:17
msgid "Document to open."
msgstr "المستند لفتحه."

#: statisticsview.cpp:50
msgid "Tasks so far:"
msgstr "المهمات لحد الآن:"

#: statisticsview.cpp:56
msgid "This is the current total number of solved tasks."
msgstr "هذا العدد الكلي الحالي من المهام المحلولة."

#: statisticsview.cpp:59
msgid "Correct:"
msgstr "الصحيح:"

#: statisticsview.cpp:76
msgid "This is the current total number of correctly solved tasks."
msgstr "هذا العدد الكلي الحالي من المهام المحلولة الصحيحة."

#: statisticsview.cpp:79
msgid "Incorrect:"
msgstr "خاطئ:"

#: statisticsview.cpp:96
msgid "This is the current total number of unsolved tasks."
msgstr "هذا العدد الكلي الحالي من المهام غير المحلولة."

#: statisticsview.cpp:100
msgid "&Reset"
msgstr "إ&عادة تعيين"

#: statisticsview.cpp:103
msgid "Press the button to reset the statistics."
msgstr "إضغط على الزر لإعادة تعيين الإحصائيات."

#: testwizardview.cpp:51 testwizardview.cpp:150
msgid "Ch&eck"
msgstr "&إفحص"

#: testwizardview.cpp:54 testwizardview.cpp:121
msgid "Press the button to start the test"
msgstr "إاضغط على الزر لبدء الاختبار"

#: testwizardview.cpp:60
msgid "&Stop"
msgstr "&قف"

#: testwizardview.cpp:63
msgid "Press the button to stop the test"
msgstr "إضغط على الزر لإيقاف الاختبار"

#: testwizardview.cpp:72
msgid "Press the button to open the handbook"
msgstr "إضغظ الزر لفتح الكتيب اليدوي"

#: testwizardview.cpp:135
msgid "N&ext Task"
msgstr "المهمّة الت&الية"

#: testwizardview.cpp:136
msgid "Press the button to go to the next task."
msgstr "إضغظ على الزر للانتقال إلى المهمة التالية"

#: testwizardview.cpp:151
msgid "Press the button to check your results"
msgstr "إضغظ على الزر لفحص نتائجك"
