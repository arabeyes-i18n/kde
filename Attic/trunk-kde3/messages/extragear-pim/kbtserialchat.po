# translation of kbtserialchat.po to Arabic
#
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kbtserialchat\n"
"POT-Creation-Date: 2005-05-12 01:27+0200\n"
"PO-Revision-Date: 2006-11-17 15:27+0100\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#: main.cpp:23
msgid "A KDE KPart Application"
msgstr ""

#: main.cpp:29 main.cpp:30 main.cpp:31
msgid "Used by kbluetoothd"
msgstr ""

#: main.cpp:32
msgid "Used by kio_sdp"
msgstr ""

#. i18n: file maindialogbase.ui line 16
#: main.cpp:41 rc.cpp:3 rc.cpp:15
#, no-c-format
msgid "Bluetooth Serial Chat"
msgstr ""

#. i18n: file maindialogbase.ui line 27
#: rc.cpp:6 rc.cpp:18
#, no-c-format
msgid "<font color=\"#0000ff\" size=\"+2\">Bluetooth Serial Chat</font>"
msgstr ""

#. i18n: file maindialogbase.ui line 54
#: rc.cpp:9 rc.cpp:21
#, no-c-format
msgid "&Send"
msgstr "&أرسل"

#. i18n: file maindialogbase.ui line 57
#: rc.cpp:12 rc.cpp:24
#, no-c-format
msgid "Alt+S, Return"
msgstr ""
