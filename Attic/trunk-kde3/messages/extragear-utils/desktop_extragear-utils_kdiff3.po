# translation of desktop_extragear-utils_kdiff3.po to Arabic
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002.
# Nuriddin S. Aminagha <nuriddin@haydarlinux.org>, 2003.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# #-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#
# translation of desktop_kdeextragear-1.po to
# #-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#
msgid ""
msgstr ""
"Project-Id-Version: desktop_extragear-utils_kdiff3\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2007-03-10 07:42+0000\n"
"PO-Revision-Date: 2006-12-17 15:30+0100\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#\n"
"X-Generator: KBabel 1.11.4\n"
"#-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#\n"
"X-Generator: KBabel 1.11.4\n"

#: kdiff3plugin/kdiff3_plugin.desktop:4
msgid "Name=Compare/Merge Files/Directories"
msgstr "Name=قارن/أدمج ملفات/مجلّدات"

#: kdiff3plugin/kdiff3plugin.desktop:4
msgid "Name=Compare/Merge Files/Directories with KDiff3"
msgstr "Name=قارن/أدمج ملفات/مجلّدات بواسطة KDiff3"

#: src/kdiff3.desktop:4
msgid "Name=KDiff3"
msgstr "Name=KDiff3"

#: src/kdiff3.desktop:9
msgid "GenericName=Diff/Patch Frontend"
msgstr "GenericName=واجهة أمامية للبرامج Diff/Patch"

#: src/kdiff3.desktop:53
msgid "Comment=A File And Directory Comparison And Merge Tool"
msgstr "Comment=أداة مقارنة  و دمج ملفات و دلائل (مجلًدات )"

#: src/kdiff3part.desktop:3
msgid "Name=KDiff3Part"
msgstr "Name=KDiff3Part"
