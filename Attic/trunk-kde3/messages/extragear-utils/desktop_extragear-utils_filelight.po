# translation of desktop_extragear-utils_filelight.po to Arabic
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002.
# Nuriddin S. Aminagha <nuriddin@haydarlinux.org>, 2003.
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# #-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#
# translation of desktop_kdeextragear-1.po to
# #-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#
msgid ""
msgstr ""
"Project-Id-Version: desktop_extragear-utils_filelight\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2007-03-10 07:42+0000\n"
"PO-Revision-Date: 2006-09-04 17:25+0200\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <ar@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#\n"
"X-Generator: KBabel 1.11.2\n"
"#-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#\n"
"X-Generator: KBabel 1.11.2\n"

#: misc/filelight.desktop:4
msgid "Name=Filelight"
msgstr "Name=Filelight"

#: misc/filelight.desktop:6
#, fuzzy
msgid "GenericName=Disk Usage Information"
msgstr "Comment=عرض  معلومات حول استعمال القرص"

#: misc/filelight.desktop:25
msgid "Comment=View disk usage information"
msgstr "Comment=عرض  معلومات حول استعمال القرص"

#: misc/filelight_part.desktop:3
msgid "Name=RadialMap View"
msgstr "Name=عرض RadialMap"
