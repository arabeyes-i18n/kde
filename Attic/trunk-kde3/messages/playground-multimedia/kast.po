# translation of kast.po to Arabic
#
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kast\n"
"POT-Creation-Date: 2005-03-23 01:26+0100\n"
"PO-Revision-Date: 2006-10-26 18:12+0200\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <ar@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#: kast.cpp:55
msgid "Could not find our part."
msgstr ""

#: kast_part.cpp:51
msgid "&Stroke Dock"
msgstr ""

#: kast_part.cpp:57
msgid "&Record Animation"
msgstr ""

#: kast_part.cpp:269
msgid "kastPart"
msgstr "kastPart"

#: main.cpp:9
msgid "A KDE KPart Application"
msgstr ""

#: main.cpp:15
msgid "Document to open"
msgstr "المستند للفتح"

#: main.cpp:21
msgid "kast"
msgstr "kast"

#. i18n: file kast_shell.rc line 16
#: rc.cpp:12
#, no-c-format
msgid "&Animation"
msgstr ""
