# translation of kipiplugin_gpssync.po to Arabic
#
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: kipiplugin_gpssync\n"
"POT-Creation-Date: 2007-03-13 08:54+0100\n"
"PO-Revision-Date: 2006-11-14 20:08+0100\n"
"Last-Translator: محمد سعد  Mohamed SAAD <metehyi@free.fr>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: _translatorinfo.cpp:1
msgid ""
"_: NAME OF TRANSLATORS\n"
"Your names"
msgstr ""

#: _translatorinfo.cpp:3
msgid ""
"_: EMAIL OF TRANSLATORS\n"
"Your emails"
msgstr ""

#: gpseditdialog.cpp:78
msgid "%1 - Edit Geographical Coordinates"
msgstr ""

#: gpseditdialog.cpp:86
msgid ""
"<p>Use the map on the right to select the place where the picture have been "
"taken. Click with right mouse button on the map to get the GPS coordinates."
"<p>"
msgstr ""

#: gpseditdialog.cpp:90
msgid "Altitude:"
msgstr "الإرتفاع:"

#: gpseditdialog.cpp:91
msgid "Latitude:"
msgstr ""

#: gpseditdialog.cpp:92
msgid "Longitude:"
msgstr ""

#: gpseditdialog.cpp:143 gpssyncdialog.cpp:251 gpssyncdialog.cpp:311
#: gpssyncdialog.cpp:319 gpssyncdialog.cpp:434 gpssyncdialog.cpp:444
#: gpssyncdialog.cpp:453 gpssyncdialog.cpp:485
msgid "GPS Sync"
msgstr ""

#: gpseditdialog.cpp:146 gpssyncdialog.cpp:254
msgid "A Plugin to synchronize pictures metadata with a GPS device"
msgstr ""

#: gpseditdialog.cpp:149 gpssyncdialog.cpp:257
msgid "Author and Maintainer"
msgstr ""

#: gpseditdialog.cpp:154 gpssyncdialog.cpp:262
msgid "GPS Sync Handbook"
msgstr ""

#: gpseditdialog.cpp:239
msgid "Altitude value is not correct!"
msgstr ""

#: gpseditdialog.cpp:240 gpseditdialog.cpp:248 gpseditdialog.cpp:256
#: plugin_gpssync.cpp:244
msgid "Edit Geographical Coordinates"
msgstr ""

#: gpseditdialog.cpp:247
msgid "Latitude value is not correct!"
msgstr ""

#: gpseditdialog.cpp:255
msgid "Longitude value is not correct!"
msgstr ""

#: gpslistviewitem.cpp:87
msgid "Read only"
msgstr "للقراءة فقط"

#: gpslistviewitem.cpp:126
msgid "Interpolated"
msgstr ""

#: gpslistviewitem.cpp:130
msgid "Added"
msgstr ""

#: gpslistviewitem.cpp:132
msgid "Found"
msgstr ""

#: gpslistviewitem.cpp:152
msgid "Deleted!"
msgstr ""

#: gpslistviewitem.cpp:166
msgid "Not available"
msgstr "غير متوفر"

#: gpssyncdialog.cpp:105 plugin_gpssync.cpp:68
msgid "Geolocalization"
msgstr ""

#: gpssyncdialog.cpp:112
msgid "Correlate"
msgstr ""

#: gpssyncdialog.cpp:113
msgid "Edit..."
msgstr "حرر..."

#: gpssyncdialog.cpp:116
msgid "Correlate GPX file data with all pictures from the list."
msgstr ""

#: gpssyncdialog.cpp:117
msgid "Edit manually GPS coordinates of selected pictures from the list."
msgstr ""

#: gpssyncdialog.cpp:118
msgid "Remove GPS coordinates of selected pictures from the list."
msgstr ""

#: gpssyncdialog.cpp:136
msgid "Pictures Geolocalization"
msgstr ""

#: gpssyncdialog.cpp:152
msgid "Thumbnail"
msgstr ""

#: gpssyncdialog.cpp:153
msgid "File Name"
msgstr "إسم الملف"

#: gpssyncdialog.cpp:154
msgid "Date Taken"
msgstr ""

#: gpssyncdialog.cpp:155
msgid "Latitude"
msgstr ""

#: gpssyncdialog.cpp:156
msgid "Longitude"
msgstr ""

#: gpssyncdialog.cpp:157
msgid "Altitude"
msgstr ""

#: gpssyncdialog.cpp:158
msgid "Status"
msgstr "الحالة"

#: gpssyncdialog.cpp:168
msgid "Settings"
msgstr "التعيينات"

#: gpssyncdialog.cpp:172
msgid "Load GPX File..."
msgstr ""

#: gpssyncdialog.cpp:174
msgid "Current GPX file:"
msgstr ""

#: gpssyncdialog.cpp:175
msgid "No GPX file"
msgstr ""

#: gpssyncdialog.cpp:179
msgid "Max. time gap:"
msgstr ""

#: gpssyncdialog.cpp:181
msgid ""
"<p>Sets the maximum difference in seconds from a GPS track point to the image "
"time to be matched. If the time difference exceeds this setting, no match takes "
"place."
msgstr ""

#: gpssyncdialog.cpp:185
msgid "Time zone:"
msgstr ""

#: gpssyncdialog.cpp:187
msgid "GMT-12:00"
msgstr ""

#: gpssyncdialog.cpp:188
msgid "GMT-11:00"
msgstr ""

#: gpssyncdialog.cpp:189
msgid "GMT-10:00"
msgstr ""

#: gpssyncdialog.cpp:190
msgid "GMT-09:00"
msgstr ""

#: gpssyncdialog.cpp:191
msgid "GMT-08:00"
msgstr ""

#: gpssyncdialog.cpp:192
msgid "GMT-07:00"
msgstr ""

#: gpssyncdialog.cpp:193
msgid "GMT-06:00"
msgstr ""

#: gpssyncdialog.cpp:194
msgid "GMT-05:00"
msgstr ""

#: gpssyncdialog.cpp:195
msgid "GMT-04:00"
msgstr ""

#: gpssyncdialog.cpp:196
msgid "GMT-03:00"
msgstr ""

#: gpssyncdialog.cpp:197
msgid "GMT-02:00"
msgstr ""

#: gpssyncdialog.cpp:198
msgid "GMT-01:00"
msgstr ""

#: gpssyncdialog.cpp:199
msgid "GMT"
msgstr ""

#: gpssyncdialog.cpp:200
msgid "GMT+01:00"
msgstr ""

#: gpssyncdialog.cpp:201
msgid "GMT+02:00"
msgstr ""

#: gpssyncdialog.cpp:202
msgid "GMT+03:00"
msgstr ""

#: gpssyncdialog.cpp:203
msgid "GMT+04:00"
msgstr ""

#: gpssyncdialog.cpp:204
msgid "GMT+05:00"
msgstr ""

#: gpssyncdialog.cpp:205
msgid "GMT+06:00"
msgstr ""

#: gpssyncdialog.cpp:206
msgid "GMT+07:00"
msgstr ""

#: gpssyncdialog.cpp:207
msgid "GMT+08:00"
msgstr ""

#: gpssyncdialog.cpp:208
msgid "GMT+09:00"
msgstr ""

#: gpssyncdialog.cpp:209
msgid "GMT+10:00"
msgstr ""

#: gpssyncdialog.cpp:210
msgid "GMT+11:00"
msgstr ""

#: gpssyncdialog.cpp:211
msgid "GMT+12:00"
msgstr ""

#: gpssyncdialog.cpp:212
msgid "GMT+13:00"
msgstr ""

#: gpssyncdialog.cpp:213
msgid "GMT+14:00"
msgstr ""

#: gpssyncdialog.cpp:214
msgid ""
"<p>Sets the time zone of the camera during picture shooting, so that the time "
"stamps of the pictures can be converted to GMT to match the GPS time"
msgstr ""

#: gpssyncdialog.cpp:218
msgid "Interpolate"
msgstr ""

#: gpssyncdialog.cpp:219
msgid ""
"<p>Set this option to interpolate GPS track points which are not closely "
"matched to the GPX data file."
msgstr ""

#: gpssyncdialog.cpp:222
msgid "Difference in min.:"
msgstr ""

#: gpssyncdialog.cpp:224
msgid ""
"<p>Sets the maximum time difference in minutes (240 max.) to interpolate GPX "
"file points to image time data."
msgstr ""

#: gpssyncdialog.cpp:300
msgid "%1|GPS Exchange Format"
msgstr ""

#: gpssyncdialog.cpp:301
msgid "Select GPX File to Load"
msgstr ""

#: gpssyncdialog.cpp:310
msgid "Cannot parse %1 GPX file!"
msgstr ""

#: gpssyncdialog.cpp:318
msgid "The %1 GPX file do not have a date-time track to use!"
msgstr ""

#: gpssyncdialog.cpp:325
#, c-format
msgid "Points parsed: %1"
msgstr ""

#: gpssyncdialog.cpp:369
#, c-format
msgid ""
"_n: 1 picture from the list isn't updated.\n"
"%n pictures from the list isn't updated."
msgstr ""

#: gpssyncdialog.cpp:372
msgid ""
"<p>%1\n"
"Do you want really to close this window without applying changes?</p>"
msgstr ""

#: gpssyncdialog.cpp:433
msgid "Cannot find pictures to correlate with GPX file data."
msgstr ""

#: gpssyncdialog.cpp:439
#, c-format
msgid ""
"_n: GPS data of 1 picture have been updated on the list using the GPX data "
"file.\n"
"GPS data of %n pictures have been updated on the list using the GPX data file."
msgstr ""

#: gpssyncdialog.cpp:442
msgid "Press Apply button to update pictures metadata."
msgstr ""

#: gpssyncdialog.cpp:452
msgid ""
"Please, select at least one picture from the list to edit GPS coordinates "
"manually."
msgstr ""

#: gpssyncdialog.cpp:484
msgid ""
"Please, select at least one picture from the list to remove GPS coordinates."
msgstr ""

#: plugin_gpssync.cpp:73
msgid "Correlator..."
msgstr ""

#: plugin_gpssync.cpp:81
msgid "Edit Coordinates..."
msgstr ""

#: plugin_gpssync.cpp:89
msgid "Remove Coordinates..."
msgstr ""

#: plugin_gpssync.cpp:123
msgid ""
"<qt>"
"<p>Unable to find the gpsbabel executable:"
"<br> This program is required by this plugin to support GPS data file decoding. "
"Please install gpsbabel as a package from your distributor or <a href=\"%1\">"
"download the source</a>.</p>"
"<p>Note: at least, gpsbabel version %2 is required by this plugin.</p></qt>"
msgstr ""

#: plugin_gpssync.cpp:140
msgid ""
"<qt>"
"<p>gpsbabel executable isn't up to date:"
"<br> The version %1 of gpsbabel have been found on your computer. This version "
"is too old to run properly with this plugin. Please update gpsbabel as a "
"package from your distributor or <a href=\"%2\">download the source</a>.</p>"
"<p>Note: at least, gpsbabel version %3 is required by this plugin</p></qt>"
msgstr ""

#: plugin_gpssync.cpp:242
msgid "Unable to save geographical coordinates into:"
msgstr ""

#: plugin_gpssync.cpp:258
msgid ""
"Geographical coordinates will be definitivly removed from all current selected "
"pictures.\n"
"Do you want to continue ?"
msgstr ""

#: plugin_gpssync.cpp:260 plugin_gpssync.cpp:304
msgid "Remove Geographical Coordinates"
msgstr ""

#: plugin_gpssync.cpp:302
msgid "Unable to remove geographical coordinates from:"
msgstr ""
