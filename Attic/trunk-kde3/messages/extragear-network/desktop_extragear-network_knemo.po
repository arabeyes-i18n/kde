# #-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#
# translation of desktop_kdeextragear-1.po to 
# Isam Bayazidi <bayazidi@arabeyes.org>, 2002
# #-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#
# translation of desktop_kdeextragear-2.po to Arabic
# Nuriddin S. Aminagha <nuriddin@haydarlinux.org>, 2003
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: desktop_kdeextragear-2\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2006-12-28 08:03+0000\n"
"PO-Revision-Date: 2003-07-07 01:03+0300\n"
"Last-Translator: Nuriddin S. Aminagha <nuriddin@haydarlinux.org>\n"
"Language-Team: Arabic <support@arabeyes.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"#-#-#-#-#  desktop_kdeextragear-1.po (desktop_kdeextragear-1)  #-#-#-#-#\n"
"X-Generator: KBabel 1.0\n"
"#-#-#-#-#  desktop_kdeextragear-2.po (desktop_kdeextragear-2)  #-#-#-#-#\n"
"X-Generator: KBabel 1.0\n"

#: kcm/kcm_knemo.desktop:12
#, fuzzy
msgid "Name=Network Monitor"
msgstr "GenericName=مراقب الشبكة"

#: kcm/kcm_knemo.desktop:40
msgid "Comment=Monitor network interfaces"
msgstr ""

#: kcm/kcm_knemo.desktop:69
msgid ""
"Keywords=knemo,network monitor,wlan monitor,wifi monitor,ethernet monitor,"
"wireless monitor,systray"
msgstr ""

#: knemod/eventsrc:3 knemod/knemod.desktop:13
#, fuzzy
msgid "Comment=KDE Network Monitor"
msgstr "GenericName=مراقب الشبكة"

#: knemod/eventsrc:33
msgid "Name=Connected"
msgstr ""

#: knemod/eventsrc:66
#, fuzzy
msgid "Comment=Interface is connected"
msgstr "Comment=واجهة كيدي لـ MPlayer"

#: knemod/eventsrc:98
msgid "Name=Disconnected"
msgstr ""

#: knemod/eventsrc:131
#, fuzzy
msgid "Comment=Interface is disconnected"
msgstr "Comment=واجهة كيدي لـ MPlayer"

#: knemod/eventsrc:163
#, fuzzy
msgid "Name=Not existing"
msgstr "الاسم جديد CD"

#: knemod/eventsrc:191
#, fuzzy
msgid "Comment=Interface does not exist"
msgstr "التعليق A"

#: knemod/knemod.desktop:9
#, fuzzy
msgid "Name=KNemo"
msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=Kallers"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment="
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Name=KChat"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=KFTPGrabber"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "GenericName=FTP Client"
#~ msgstr "GenericName=مراقب المعالج"

#, fuzzy
#~ msgid "Name=gFTP Import plugin"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=kmldonkey"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Comment=Disconnected from core"
#~ msgstr "Comment=واجهة كيدي لـ MPlayer"

#, fuzzy
#~ msgid "Name=Big Bullets"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=Notify"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=Query"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=Kick"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "GenericName=IRC Chat"
#~ msgstr "GenericName=مراقب المعالج"

#, fuzzy
#~ msgid "Name=Konversation"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "GenericName=A KDE front-end to smssend"
#~ msgstr "A كيدي"

#, fuzzy
#~ msgid "Name=KSMSSend"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=KTorrent"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=BitTorrent Client"
#~ msgstr "GenericName=مراقب المعالج"

#, fuzzy
#~ msgid "Comment=A BitTorrent program for KDE"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=amaroK"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=Audio Player"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=Enqueue in amaroK"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=aKode Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=aKode audio-engine for amaroK"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=aRts Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=GStreamer Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=KDEMM Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=MAS Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=NMM Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=<no engine>"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=xine Engine"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=CD Bake Oven audio copy settings"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Keywords=cdbo,audio,cdbakeoven"
#~ msgstr "كلمة ربط"

#, fuzzy
#~ msgid "Name=Audio Copy Settings"
#~ msgstr "الاسم صوت نسخ"

#, fuzzy
#~ msgid "Comment=CD Bake Oven recording options"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Keywords=cdbo,recording,burning,cdbakeoven"
#~ msgstr "كلمة ربط"

#, fuzzy
#~ msgid "Name=Recording Options"
#~ msgstr "الاسم تسجيل"

#, fuzzy
#~ msgid "Comment=Detected and configured CD-ROM devices"
#~ msgstr "التعليق و CD"

#, fuzzy
#~ msgid "Keywords=cdbo,cdrom,devices,cdbakeoven"
#~ msgstr "كلمة ربط"

#, fuzzy
#~ msgid "Name=CD-ROM Devices"
#~ msgstr "الاسم CD"

#, fuzzy
#~ msgid "Comment=CD Bake Oven default settings"
#~ msgstr "التعليق CD افتراضي"

#, fuzzy
#~ msgid "Name=Customize Defaults"
#~ msgstr "الاسم تخصيص"

#, fuzzy
#~ msgid "Comment=New CD (ISO image) settings"
#~ msgstr "التعليق جديد CD"

#, fuzzy
#~ msgid "Keywords=cdbo,iso,cdbakeoven"
#~ msgstr "كلمة ربط"

#, fuzzy
#~ msgid "Comment=CD Bake Oven settings"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=CDBO Settings"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=CD Bake Oven setup tool"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=CDBO Setup Tool"
#~ msgstr "الاسم تنصيب"

#, fuzzy
#~ msgid "Name=CD Bake Oven"
#~ msgstr "الاسم CD"

#, fuzzy
#~ msgid "Comment=Audio and data CD writing program"
#~ msgstr "التعليق صوت و بيانات CD"

#, fuzzy
#~ msgid "GenericName=CD Burning Program"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=Create CD..."
#~ msgstr "الاسم انشيء CD."

#, fuzzy
#~ msgid "Name=Create Audio CD..."
#~ msgstr "الاسم انشيء CD."

#, fuzzy
#~ msgid "Name=Save to File (Make ISO)..."
#~ msgstr "الاسم احفظ ملف صنع."

#, fuzzy
#~ msgid "Comment=Use to create musical CDs."
#~ msgstr "التعليق ��"

#, fuzzy
#~ msgid "Name=Audio CD"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=Use to create single or multi session data CDs."
#~ msgstr "التعليق ��"

#, fuzzy
#~ msgid "Name=Data CD"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=KDE CD-R(W) Watcher"
#~ msgstr "الاسم كيدي CD R"

#, fuzzy
#~ msgid "Comment=Monitors CD-R(W) Drives Media"
#~ msgstr "التعليق CD R"

#, fuzzy
#~ msgid "Name=My CDs"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=ISO Image"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Comment=CD Bake Oven Compilation File List"
#~ msgstr "التعليق CD ملف"

#, fuzzy
#~ msgid "Comment=New CD File List"
#~ msgstr "التعليق جديد CD ملف"

#, fuzzy
#~ msgid "Comment=Virtual CD Folder"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Comment=Imported Virtual CD Folder"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Comment=CD Descriptor File"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=DataKiosk"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=Database Interface"
#~ msgstr "A كيدي"

#, fuzzy
#~ msgid "Name=Gwenview"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=A simple image viewer"
#~ msgstr "التعليق A"

#, fuzzy
#~ msgid "Name=Browse with Gwenview"
#~ msgstr "الاسم استعراض مع"

#, fuzzy
#~ msgid "Name=Image View"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=Gwenview Image Viewer"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=Konqueror"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=Set as Wallpaper"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Name=K3bSetup"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=CD & DVD Burning Setup"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Comment=K3b"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Name=K3b"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=CD & DVD Burning"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=Create Audio CD with K3b..."
#~ msgstr "الاسم انشيء CD."

#, fuzzy
#~ msgid "Name=Create Data CD with K3b..."
#~ msgstr "الاسم انشيء CD."

#, fuzzy
#~ msgid "Name=Create Video CD with K3b..."
#~ msgstr "الاسم انشيء CD."

#, fuzzy
#~ msgid "Comment=K3b Project"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Name=KDiff3"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "GenericName=Diff/Patch Frontend"
#~ msgstr "A كيدي"

#, fuzzy
#~ msgid "Name=KDiff3Part"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=KFortune"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Name=Could not execute the 'fortune' program"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=KFortune failed to execute the external 'fortune' program"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Name=Could not create output file"
#~ msgstr "الاسم إخراج"

#, fuzzy
#~ msgid "Comment=KFortune failed to create the output file to save the text"
#~ msgstr "التعليق إخراج"

#, fuzzy
#~ msgid "Comment=Duplicate a CD"
#~ msgstr "التعليق ��"

#, fuzzy
#~ msgid "Comment=Classic mode (full featured)"
#~ msgstr "التعليق نظام"

#, fuzzy
#~ msgid "Comment=Classic mode (Audio only CD)"
#~ msgstr "التعليق نظام صوت CD"

#, fuzzy
#~ msgid "Name=Create ISO Image"
#~ msgstr "الاسم انشيء"

#, fuzzy
#~ msgid "Comment=CD recording frontend"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Comment=A KreateCD track file"
#~ msgstr "التعليق A"

#, fuzzy
#~ msgid "Name=KreateCD Track Viewer"
#~ msgstr "الاسم مسار"

#~ msgid "Name=KCPULoad"
#~ msgstr "Name=KCPULoad"

#~ msgid "GenericName=CPU Monitor"
#~ msgstr "GenericName=مراقب المعالج"

#~ msgid "Comment=A small CPU usage meter for Kicker"
#~ msgstr "Comment=مقياس استهلاك المعالج لشريط كيدي"

#, fuzzy
#~ msgid "GenericName=LaTeX Frontend"
#~ msgstr "GenericName=مراقب المعالج"

#, fuzzy
#~ msgid "Name=KimDaBa"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "GenericName=Image Database"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Comment=KimDaBa import"
#~ msgstr "Comment=هيئة وسيط ميكروسوفت"

#, fuzzy
#~ msgid "Comment=MS Media Format"
#~ msgstr "Comment=هيئة وسيط ميكروسوفت"

#, fuzzy
#~ msgid "Comment=Microsoft Audio"
#~ msgstr "Comment=ميكروسوفت AVIفيديو"

#~ msgid "Comment=Microsoft AVI Video"
#~ msgstr "Comment=ميكروسوفت AVIفيديو"

#~ msgid "Comment=KDE interface for MPlayer"
#~ msgstr "Comment=واجهة كيدي لـ MPlayer"

#, fuzzy
#~ msgid "GenericName=Media Player"
#~ msgstr "Name=KMPlayer"

#~ msgid "Name=Embedded MPlayer for KDE"
#~ msgstr "Name=MPlayer مدمج كيدي"

#, fuzzy
#~ msgid "Name=KNetLoad Applet"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Comment=Network Load Applet for Kicker"
#~ msgstr "Comment= مقياس استهلاك الشبكة لشريط كيدي"

#, fuzzy
#~ msgid "GenericName=JavaScript Console"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=kjscmd"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=KFileItem"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=Process"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=Kst"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "GenericName=Data Viewer"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=mySQL"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=ODBC"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=PostgreSQL"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=SQLite3"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=SQLite"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=SQLite2"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "GenericName=Image Explorer"
#~ msgstr "Name=KMPlayer"

#, fuzzy
#~ msgid "Name=Gwenview - hack"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Comment=A simple image viewer - hack"
#~ msgstr "التعليق A"

#, fuzzy
#~ msgid "Name=Browse with Gwenview - hack"
#~ msgstr "الاسم استعراض مع"

#, fuzzy
#~ msgid "Comment=A new media player"
#~ msgstr "التعليق A"

#, fuzzy
#~ msgid "Comment=ISO9660 image file"
#~ msgstr "التعليق"

#, fuzzy
#~ msgid "Comment=Prepares the system for CD writing"
#~ msgstr "التعليق CD"

#, fuzzy
#~ msgid "Name=Unknown format"
#~ msgstr "Name=معلومات"

#, fuzzy
#~ msgid "Name=Vim Component Configuration"
#~ msgstr "الاسم الجزء"

#, fuzzy
#~ msgid "Name=Embedded Vim Component"
#~ msgstr "الاسم"

#, fuzzy
#~ msgid "Keywords=cdbo,recording,burning,cdbakeoven,Information"
#~ msgstr "كلمة ربط"

#, fuzzy
#~ msgid "Name=KSig"
#~ msgstr "الاسم"

#~ msgid "Comment=A small network usage meter for Kicker"
#~ msgstr "Comment= مقياس استهلاك الشبكة لشريط كيدي"

#, fuzzy
#~ msgid "Name=Convolution"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=Crosscorrelation"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=Lorentzian fit"
#~ msgstr "Name=KNetLoad"

#, fuzzy
#~ msgid "Name=Linear interpolation"
#~ msgstr "Name=KNetLoad"
